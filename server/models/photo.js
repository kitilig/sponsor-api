"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Photo Schema
//================================
const PhotoSchema = new Schema({
  photoURL: {
    type: String,
    unique: true,
    required: true
  },
  _userId: { 
      type: Schema.Types.ObjectId,
      required: true
  },
  photoType: {
    type: String,
    enum: ['PUBLIC', 'PRIVATE'],
    default: 'PUBLIC',
    required: true
  },
  approvalStatus: {
    type: String,
    enum: ['APPROVED', 'NOT_APPROVED'],
    default: 'NOT_APPROVED',
    required: true
  },
  likes : { type : Array , "default" : [] },
},
{
  timestamps: true
});

module.exports = mongoose.model('Photo', PhotoSchema);
