"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Message Schema
//================================
const MessageSchema = new Schema({
  messageBody: {
    type: String,
    unique: true,
    required: true
  },
  _senderId: { 
      type: Schema.Types.ObjectId,
      required: true
  },
  _recipientId: { 
      type: Schema.Types.ObjectId,
      required: true
  },
  _threadId: { 
      type: Schema.Types.ObjectId,
      required: true
  },
  messageStatus: {
    type: String,
    enum: ['READ', 'NOT_READ'],
    default: 'NOT_READ',
    required: true
  }
},
{
  timestamps: true
});

module.exports = mongoose.model('Message', MessageSchema);
