"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Payment Schema
//================================
const PaymentSchema = new Schema({
  amount: {
    type: Number,
    unique: true,
    required: true
  },
  _userId: { 
      type: Schema.Types.ObjectId,
      required: true
  }
},
{
  timestamps: true
});

module.exports = mongoose.model('Payment', PaymentSchema);
