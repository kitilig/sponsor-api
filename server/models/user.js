"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      bcrypt = require('bcrypt-nodejs');

//================================
// User Schema
//================================
const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  username: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  dateOfBirth: { 
    type: Date, 
    default: Date.now 
  },
  profile: {
    profilePicture: { type: String },
    bodyType: { type: String },
    ethnicity: { type: String },
    relationshipStatus: { type: String },
    hasChildren: { type: String },
    smoking: { type: String },
    drinking: { type: String },
    age: { type: String },
    netWorth: { type: String },
    salaryRange: { type: String },
    location: { type: String },
    description: { type: String },
    amLookingFor: { type: String },
    exerciseFrequency: { type: String },
    occupation: { type: String },
  },
  likes : { type : Array , "default" : [] },
  views : { type : Array , "default" : [] },
  favorites : { type : Array , "default" : [] },
  winks : { type : Array , "default" : [] },
  matches : { type : Array , "default" : [] },
  dislikes : { type : Array , "default" : [] },
  settings: {
    usernameVisible: { type: String },
    ageVisible: { type: String },
    locationVisible: { type: String },
    receiveAlerts: { type: String },
  },
  role: {
    type: String,
    enum: ['Member', 'Admin'],
    default: 'Member'
  },
  resetPasswordToken: { type: String },
  resetPasswordExpires: { type: Date }
},
{
  timestamps: true
});

// Pre-save of user to database, hash password if password is modified or new
UserSchema.pre('save', function(next) {
  const user = this,
        SALT_FACTOR = 5;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});


// Method to compare password for login
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) { return cb(err); }

    cb(null, isMatch);
  });
}

module.exports = mongoose.model('User', UserSchema);
