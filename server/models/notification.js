"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Notification Schema
//================================
const NotificationSchema = new Schema({
  notificationBody: {
    type: String,
    unique: true,
    required: true
  },
  _userId: { 
      type: Schema.Types.ObjectId,
      required: true
  },
  notificationType: {
    type: String,
    enum: ['SYSTEM', 'USER'],
    required: true
  },
  url: {
    type: String,
  },
  notificationStatus: {
    type: String,
    enum: ['READ', 'NOT_READ'],
    default: 'NOT_READ',
    required: true
  }
},
{
  timestamps: true
});

module.exports = mongoose.model('Notification', NotificationSchema);
