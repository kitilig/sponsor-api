"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Event Schema
//================================
const EventSchema = new Schema({
  eventTitle: {
    type: String,
    required: true
  },
  eventVenue: {
    type: String,
    required: true
  },
  eventDate: { 
    type: Date, 
    default: Date.now 
  },
  eventType: {
    type: String,
    enum: ['FREE', 'INVITE_ONLY', 'OTHER'],
    default: 'FREE',
    required: true
  },
  eventBannerURL: {
    type: String,
    required: true
  }
},
{
  timestamps: true
});

module.exports = mongoose.model('Event', EventSchema);
