"use strict";

const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//================================
// Help Schema
//================================
const HelpSchema = new Schema({
  title: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    unique: true,
    required: true
  }
},
{
  timestamps: true
});

module.exports = mongoose.model('Help', HelpSchema);
