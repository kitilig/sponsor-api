"use strict";

module.exports = {
  // Secret key for JWT signing and encryption
  'secret': 'super#secret#passphrase',
  // Database connection information
  //mongodb://<dbuser>:<dbpassword>@ds031835.mlab.com:31835/saap
  'database': 'mongodb://admin:password@ds139567.mlab.com:39567/sponsor',//process.env.MONGO_URL || 'mongodb://localhost:27017',
  // Setting port for server
  'port': process.env.PORT || 8080,
}
