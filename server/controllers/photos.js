"use strict";

const Photo = require('../models/photo'),
      config = require('../config/main');

//========================================
// Photo index Route
//========================================
exports.fetchAllPhotos = function(req, res, next) {

  Photo.find({}, function(err, photos) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch photos.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Photos successfully fecthed.",
      payload: photos
    });
  });
}

//========================================
// create photo Route
//========================================
exports.createPhoto = function(req, res, next) {
  // Check for schema errors
  const photoURL = req.body.photoURL;
  const _userId = req.body._userId;
  const photoType = req.body.photoType;
  const approvalStatus = req.body.approvalStatus;

  // Return error if no email provided
  if (!photoURL) {
    return res.status(422).send({ success: false, message: 'You must enter a valid photo URL.', payload: {}});
  }

  // Return error if no email provided
  if (!_userId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _userId.', payload: {}});
  }

  // Return error if no email provided
  if (!photoType) {
    return res.status(422).send({ success: false, message: 'You must enter a valid photo Type.', payload: {}});
  }

  // Return error if full name not provided
  if (!approvalStatus) {
    return res.status(422).send({ success: false, message: 'You must enter photo approvalStatus.', payload: {}});
  }

  // create record
  let photo = new Photo({
    photoURL: photoURL,
    _userId: _userId,
    photoType: photoType,
    approvalStatus: approvalStatus,
  });

  photo.save(function(err, photo) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create photo.",
        payload: {
          photo
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Photo successfully created.",
      payload: {
        photo
      }
    });
  });
}

//========================================
// show photo Route
//========================================
exports.fetchPhoto = function(req, res, next) {
  const photoId = req.params.id;

  // Return error if no email provided
  if (!photoId) {
    return res.status(422).send({ error: 'You must enter a photoId.'});
  }

  Photo.findOne({_id: photoId}, function(err, photo) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No photo found with the given Id.",
        payload: {}
      });
    }

    if (photoId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No photo found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Photo successfully fetched.",
        payload: photo
      });
    }
  });
}


//========================================
// update photo Route
//========================================
exports.updatePhoto = function(req, res, next) {
  // Check for registration errors
  const photoURL = req.body.photoURL;
  const _userId = req.body._userId;
  const photoType = req.body.photoType;
  const approvalStatus = req.body.approvalStatus;
  const likes = req.body.likes;

  const photoId = req.params.id;

  Photo.findById(photoId, function(err, photo) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No photo found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (photoURL) {
      photo.photoURL = photoURL;
    }

    // Return error if no email provided
    if (_userId) {
      photo._userId = _userId;
    }

    // Return error if no email provided
    if (photoType) {
      photo.photoType = photoType;
    }

    // Return error if full name not provided
    if (approvalStatus) {
      photo.approvalStatus = approvalStatus;
    }

    // Return error if full name not provided
    if (likes) {
      photo.likes = likes;
    }

    // save the photo
    photo.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Photo could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Photo successfully updated.",
        payload: {
          photo
        }
      });
    });

  });
}

//========================================
// delete photo Route
//========================================
exports.deletePhoto  = function(req, res, next) {
  const photoId = req.params.id;

  // Return error if no email provided
  if (!photoId) {
    return res.status(422).send({ error: 'You must enter a valid photoId.'});
  }

  Photo.findById(photoId, function(err, photo) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No photo found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete photo
      photo.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete photo.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Photo successfully deleted.",
          payload: photo
        });
      });
    } catch(e){
      //catch exception. If photo is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete photo.",
        payload: {}
      });
    }
  });
}
