"use strict";

const Event = require('../models/event'),
      config = require('../config/main');

//========================================
// Event index Route
//========================================
exports.fetchAllEvents = function(req, res, next) {

  Event.find({}, function(err, events) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch Events.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Events successfully fecthed.",
      payload: events
    });
  });
}

//========================================
// create Event Route
//========================================
exports.createEvent = function(req, res, next) {
  // Check for schema errors
  const eventTitle = req.body.eventTitle;
  const eventVenue = req.body.eventVenue;
  const eventType = req.body.eventType;
  const eventDate = req.body.eventDate;
  const eventBannerURL = req.body.eventBannerURL;

  // Return error if no email provided
  if (!eventTitle) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Event Title.', payload: {}});
  }

  // Return error if no eventVenue provided
  if (!eventVenue) {
    return res.status(422).send({ success: false, message: 'You must enter a valid eventVenue.', payload: {}});
  }

  // Return error if no email provided
  if (!eventType) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Event Type.', payload: {}});
  }

  // Return error if full name not provided
  if (!eventDate) {
    return res.status(422).send({ success: false, message: 'You must enter Event eventDate.', payload: {}});
  }

  // create record
  let event = new Event({
    eventTitle: eventTitle,
    eventVenue: eventVenue,
    eventType: eventType,
    eventDate: eventDate,
    eventBannerURL: eventBannerURL,
  });

  event.save(function(err, event) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create Event.",
        payload: {
          event
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Event successfully created.",
      payload: {
        event
      }
    });
  });
}

//========================================
// show Event Route
//========================================
exports.fetchEvent = function(req, res, next) {
  const eventId = req.params.id;

  // Return error if no email provided
  if (!eventId) {
    return res.status(422).send({ error: 'You must enter a eventId.'});
  }

  Event.findOne({_id: eventId}, function(err, event) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Event found with the given Id.",
        payload: {}
      });
    }

    if (eventId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No Event found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Event successfully fetched.",
        payload: event
      });
    }
  });
}


//========================================
// update Event Route
//========================================
exports.updateEvent = function(req, res, next) {
  // Check for registration errors
  const eventTitle = req.body.eventTitle;
  const eventVenue = req.body.eventVenue;
  const eventType = req.body.eventType;
  const eventDate = req.body.eventDate;
  const eventBannerURL = req.body.eventBannerURL;

  const eventId = req.params.id;

  Event.findById(eventId, function(err, event) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Event found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (eventTitle) {
      event.eventTitle = eventTitle;
    }

    // Return error if no eventVenue provided
    if (eventVenue) {
      event.eventVenue = eventVenue;
    }

    // Return error if no email provided
    if (eventType) {
      event.eventType = eventType;
    }

    // Return error if full name not provided
    if (eventDate) {
      event.eventDate = eventDate;
    }

    // Return error if full name not provided
    if (eventBannerURL) {
      event.eventBannerURL = eventBannerURL;
    }

    // save the Event
    event.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Event could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Event successfully updated.",
        payload: {
          event
        }
      });
    });

  });
}

//========================================
// delete Event Route
//========================================
exports.deleteEvent  = function(req, res, next) {
  const eventId = req.params.id;

  // Return error if no email provided
  if (!eventId) {
    return res.status(422).send({ error: 'You must enter a valid EventId.'});
  }

  Event.findById(eventId, function(err, event) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Event found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete Event
      event.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete Event.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Event successfully deleted.",
          payload: event
        });
      });
    } catch(e){
      //catch exception. If Event is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete Event.",
        payload: {}
      });
    }
  });
}
