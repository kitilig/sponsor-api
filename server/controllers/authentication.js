"use strict";

const jwt = require('jsonwebtoken'),
      crypto = require('crypto'),
      User = require('../models/user'),
      config = require('../config/main'),
      bodyParser = require('body-parser');

/**
 * Generate a JSON web token from the user object we pass in.
 */
function generateToken(user) {
  return jwt.sign(user, config.secret, {
    expiresIn: 10080 // in seconds
  });
}

/**
 * Set user info from request:
 *
 * We don't want to use the entire user object to sign our JWTs-- that's a lot
 * of information to eventually store in a cookie. Plus, we don't want to be
 * returning huge blocks of what could be sensitive user information.
 */
function setUserInfo(request) {
  return {
    _id: request._id,
    profile: {
      firstName: request.profile.firstName,
      lastName: request.profile.lastName,
    },
    email: request.email,
    role: request.role,
  };
}

//========================================
// Login Route
//========================================
exports.login = function(req, res, next) {

  let userInfo = setUserInfo(req.user);

  res.status(200).json({
    success: true,
    message: "User successfully logged in.",
    payload:{
      token: 'JWT ' + generateToken(userInfo),
      user: userInfo
    }
  });
}


//========================================
// Registration Route
//========================================
exports.register = function(req, res, next) {
  // Check for registration errors
  const email = req.body.email;
  const username = req.body.username;
  const dateOfBirth = req.body.dateOfBirth;
  const password = req.body.password;

  // Return error if no email provided
  if (!email) {
    return res.status(422).send({ success: false, message: 'You must enter an email address.', payload: {}});
  }

  // Return error if full name not provided
  if (!username) {
    return res.status(422).send({ success: false, message: 'You must enter your username.', payload: {}});
  }

  // Return error if dateOfBirth not provided
  if (!dateOfBirth) {
    return res.status(422).send({ success: false, message: 'You must enter your dateOfBirth.', payload: {}});
  }

  // Return error if no password provided
  if (!password) {
    return res.status(422).send({ success: false, message: 'You must enter a password.', payload: {} });
  }

  User.findOne({ email: email }, function(err, existingUser) {
      if (err) { return next(err); }

      // If user is not unique, return error
      if (existingUser) {
        return res.status(422).send({ success: false, message: 'That email address is already in use.', payload: {} });
      }

      // If email is unique and password was provided, create account
      let user = new User({
        email: email,
        password: password,
        username: username,
        dateOfBirth: dateOfBirth
      });

      user.save(function(err, user) {
        if (err) { return next(err); }

        // Subscribe member to Mailchimp list
        // mailchimp.subscribeToNewsletter(user.email);

        // Respond with JWT if user was created

        let userInfo = setUserInfo(user);

        res.status(201).json({
          success: true,
          message: "User successfully created.",
          payload: {
            token: 'JWT ' + generateToken(userInfo),
            user: userInfo
          }
        });
      });
  });
}

//========================================
// Authorization Middleware
//========================================

// Role authorization check
exports.roleAuthorization = function(role) {
  return function(req, res, next) {
    const user = req.user;

    User.findById(user._id, function(err, foundUser) {
      if (err) {
        res.status(422).json({ error: 'No user was found.' });
        return next(err);
      }

      // If user is found, check role.
      if (foundUser.role == role) {
        return next();
      }

      res.status(401).json({ error: 'You are not authorized to view this content.' });
      return next('Unauthorized');
    })
  }
}
