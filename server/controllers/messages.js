"use strict";

const Message = require('../models/message'),
      config = require('../config/main');

//========================================
// Message index Route
//========================================
exports.fetchAllMessages = function(req, res, next) {

  Message.find({}, function(err, messages) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch Messages.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Messages successfully fecthed.",
      payload: messages
    });
  });
}

//========================================
// create Message Route
//========================================
exports.createMessage = function(req, res, next) {
  // Check for schema errors
  const messageBody = req.body.messageBody;
  const _senderId = req.body._senderId;
  const _recipientId = req.body._recipientId;
  const _threadId = req.body._threadId;
  const messageStatus = req.body.messageStatus;
  const url = req.body.url;

  // Return error if no email provided
  if (!MessageBody) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Message body.', payload: {}});
  }

  // Return error if no email provided
  if (!_senderId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _senderId.', payload: {}});
  }

  // Return error if no email provided
  if (!_recipientId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _recipientId.', payload: {}});
  }

   // Return error if no email provided
  if (!_threadId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _threadId.', payload: {}});
  }

  // Return error if full name not provided
  if (!messageStatus) {
    return res.status(422).send({ success: false, message: 'You must enter Message Status.', payload: {}});
  }

  // create record
  let message = new Message({
    messageBody: messageBody,
    _senderId: _senderId,
    _recipientId: _recipientId,
    _threadId: _threadId,
    messageStatus: messageStatus,
    url: url
  });

  message.save(function(err, message) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create Message.",
        payload: {
          message
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Message successfully created.",
      payload: {
        message
      }
    });
  });
}

//========================================
// show Message Route
//========================================
exports.fetchMessage = function(req, res, next) {
  const messageId = req.params.id;

  // Return error if no email provided
  if (!messageId) {
    return res.status(422).send({ error: 'You must enter a MessageId.'});
  }

  Message.findOne({_id: messageId}, function(err, message) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Message found with the given Id.",
        payload: {}
      });
    }

    if (MessageId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No Message found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Message successfully fetched.",
        payload: message
      });
    }
  });
}


//========================================
// update Message Route
//========================================
exports.updateMessage = function(req, res, next) {
  // Check for registration errors
  const messageBody = req.body.messageBody;
  const _senderId = req.body._senderId;
  const _recipientId = req.body._recipientId;
  const _threadId = req.body._threadId;
  const messageStatus = req.body.messageStatus;

  const messageId = req.params.id;

  Message.findById(MessageId, function(err, message) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Message found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (messageBody) {
      message.messageBody = messageBody;
    }

    // Return error if no email provided
    if (_senderId) {
      message._senderId = _senderId;
    }

    // Return error if no email provided
    if (_recipientId) {
      message._recipientId = _recipientId;
    }

    // Return error if no email provided
    if (_threadId) {
      message._threadId = _threadId;
    }

    // Return error if full name not provided
    if (messageStatus) {
      message.messageStatus = essageStatus;
    }

    // save the Message
    message.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Message could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Message successfully updated.",
        payload: {
          message
        }
      });
    });

  });
}

//========================================
// delete Message Route
//========================================
exports.deleteMessage  = function(req, res, next) {
  const messageId = req.params.id;

  // Return error if no email provided
  if (!messageId) {
    return res.status(422).send({ error: 'You must enter a valid MessageId.'});
  }

  Message.findById(MessageId, function(err, message) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Message found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete Message
      message.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete Message.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Message successfully deleted.",
          payload: message
        });
      });
    } catch(e){
      //catch exception. If Message is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete Message.",
        payload: {}
      });
    }
  });
}
