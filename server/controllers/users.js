"use strict";

const jwt = require('jsonwebtoken'),
      crypto = require('crypto'),
      User = require('../models/user'),
      config = require('../config/main'),
      bodyParser = require('body-parser');

//========================================
// Users index Route
//========================================

exports.fetchAllUsers = function(req, res, next) {

  User.find({}, function(err, users) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch users.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Users successfully fecthed.",
      payload: users
    });
  });
}

//========================================
// Show user Route
//========================================
exports.fetchUser = function(req, res, next) {
  const userId = req.params.id;

  // Return error if no email provided
  if (!userId) {
    return res.status(422).send({ error: 'You must enter a user_id.'});
  }

  User.findOne({_id: userId}, function(err, user) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No user found with the given Id.",
        payload: {}
      });
    }

    if (user == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No user found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "User successfully fetched.",
        payload: user
      });
    }
  });
}

//========================================
// Update user Route
//========================================
exports.updateUser = function(req, res, next) {
  // Check for registration errors
  const email = req.body.email;
  const username = req.body.username;
  const dateOfBirth = req.body.dateOfBirth;
  const password = req.body.password;
  const userId = req.params.id;

  // update optional user params
  const profile = req.body.profile;
  const likes = req.body.likes;
  const favorites = req.body.favorites;
  const winks = req.body.winks;
  const matches = req.body.matches;
  const dislikes = req.body.dislikes;
  const settings = req.body.matches;
  const views = req.body.views;

  User.findById(userId, function(err, user) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No user found with the given Id.",
        payload: {}
      });
    }

    //update email
    if (email) {
      user.email = email;
    }

    //update username
    if (username) {
      user.username = username;
    }

    //update dateOfBirth
    if (dateOfBirth) {
      user.dateOfBirth = dateOfBirth;
    }

    //update lastName
    if (password) {
      user.password = password;
    }

    // TODO update other user params
    //update profile. TODO. Handle specific params
    if (profile) {
      //user.profile = profile;
    }

    //update likes
    if (likes) {
      //user.likes = likes;
    }

    //update views
    if (views) {
      //user.views = views;
    }

    //update favorites
    if (favorites) {
      //user.favorites = favorites;
    }

    //update winks
    if (winks) {
      //user.winks = winks;
    }

    //update matches
    if (matches) {
      //user.matches = matches;
    }

    //update dislikes
    if (dislikes) {
      //user.dislikes = dislikes;
    }

    //update matches. TODO. Handle specific params
    if (settings) {
      //user.settings = settings;
    }

    // end optional updates.

    // save the user
    user.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. User account could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "User successfully updated.",
        payload: {
          user
        }
      });
    });

  });
}

//========================================
// Delete user Route
//========================================
exports.deleteUser  = function(req, res, next) {
  const userId = req.params.id;

  // Return error if no email provided
  if (!userId) {
    return res.status(422).send({ error: 'You must enter a user_id.'});
  }

  User.findById(userId, function(err, user) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No user found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete user
      user.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete user.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "User successfully deleted.",
          payload: user
        });
      });
    } catch(e){
      //catch exception. If user is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete user.",
        payload: {}
      });
    }
  });
}

//===========================================================================================================
// Business Logic of the Sponsor App                                                                       //
//===========================================================================================================

//========================================
// filterUsers(params) api
//========================================
exports.filterUsers  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// sortUsers(params) api
//========================================
exports.sortUsers  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// favoriteUser(params) api
//========================================
exports.favoriteUser  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// likeUser(params) api
//========================================
exports.likeUser  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// winkAtUser(params) api
//========================================
exports.winkAtUser  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// userMatches(params) api
//========================================
exports.userMatches  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}

//========================================
// matchUser(params) api
//========================================
exports.matchUser  = function(req, res, next) {
  // params
  //const userId = req.params.id;

  return res.status(422).json({
    success: false,
    message: "Placeholder implementation of method ",
    payload: {}
  });
}