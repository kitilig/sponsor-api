"use strict";

const Notification = require('../models/notification'),
      config = require('../config/main');

//========================================
// Notification index Route
//========================================
exports.fetchAllNotifications = function(req, res, next) {

  Notification.find({}, function(err, notifications) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch Notifications.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Notifications successfully fecthed.",
      payload: notifications
    });
  });
}

//========================================
// create Notification Route
//========================================
exports.createNotification = function(req, res, next) {
  // Check for schema errors
  const notificationBody = req.body.notificationBody;
  const _userId = req.body._userId;
  const notificationType = req.body.notificationType;
  const notificationStatus = req.body.notificationStatus;
  const url = req.body.url;

  // Return error if no email provided
  if (!notificationBody) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Notification body.', payload: {}});
  }

  // Return error if no email provided
  if (!_userId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _userId.', payload: {}});
  }

  // Return error if no email provided
  if (!notificationType) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Notification Type.', payload: {}});
  }

  // Return error if full name not provided
  if (!notificationStatus) {
    return res.status(422).send({ success: false, message: 'You must enter notification Status.', payload: {}});
  }

  // Return error if full name not provided
  if (!url) {
    return res.status(422).send({ success: false, message: 'You must enter notification url.', payload: {}});
  }

  // create record
  let notification = new Notification({
    notificationBody: notificationBody,
    _userId: _userId,
    notificationType: notificationType,
    notificationStatus: notificationStatus,
    url: url
  });

  notification.save(function(err, notification) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create Notification.",
        payload: {
          notification
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Notification successfully created.",
      payload: {
        notification
      }
    });
  });
}

//========================================
// show Notification Route
//========================================
exports.fetchNotification = function(req, res, next) {
  const notificationId = req.params.id;

  // Return error if no email provided
  if (!notificationId) {
    return res.status(422).send({ error: 'You must enter a NotificationId.'});
  }

  Notification.findOne({_id: notificationId}, function(err, notification) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Notification found with the given Id.",
        payload: {}
      });
    }

    if (notificationId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No Notification found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Notification successfully fetched.",
        payload: notification
      });
    }
  });
}


//========================================
// update Notification Route
//========================================
exports.updateNotification = function(req, res, next) {
  // Check for registration errors
  const notificationBody = req.body.notificationBody;
  const _userId = req.body._userId;
  const notificationType = req.body.notificationType;
  const notificationStatus = req.body.notificationStatus;
  const url = req.body.url;

  const notificationId = req.params.id;

  Notification.findById(notificationId, function(err, notification) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Notification found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (notificationBody) {
      notification.notificationBody = notificationBody;
    }

    // Return error if no email provided
    if (_userId) {
      notification._userId = _userId;
    }

    // Return error if no email provided
    if (notificationType) {
      notification.notificationType = notificationType;
    }

    // Return error if full name not provided
    if (notificationStatus) {
      notification.notificationStatus = notificationStatus;
    }

    // Return error if full name not provided
    if (url) {
      notification.url = url;
    }

    // save the Notification
    notification.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Notification could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Notification successfully updated.",
        payload: {
          notification
        }
      });
    });

  });
}

//========================================
// delete Notification Route
//========================================
exports.deleteNotification  = function(req, res, next) {
  const notificationId = req.params.id;

  // Return error if no email provided
  if (!notificationId) {
    return res.status(422).send({ error: 'You must enter a valid NotificationId.'});
  }

  Notification.findById(notificationId, function(err, notification) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Notification found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete Notification
      Notification.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete Notification.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Notification successfully deleted.",
          payload: notification
        });
      });
    } catch(e){
      //catch exception. If Notification is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete Notification.",
        payload: {}
      });
    }
  });
}
