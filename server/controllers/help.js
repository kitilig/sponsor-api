"use strict";

const Help = require('../models/help'),
      config = require('../config/main');

//========================================
// Help index Route
//========================================
exports.fetchAllHelp = function(req, res, next) {

  Help.find({}, function(err, help) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch Help.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Help successfully fecthed.",
      payload: help
    });
  });
}

//========================================
// create Help Route
//========================================
exports.createHelp = function(req, res, next) {
  // Check for schema errors
  const title = req.body.title;
  const description = req.body.description;

  // Return error if no email provided
  if (!title) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Help title.', payload: {}});
  }

  // Return error if no email provided
  if (!description) {
    return res.status(422).send({ success: false, message: 'You must enter a valid help description.', payload: {}});
  }

  // create record
  let help = new Help({
    title: title,
    description: description,
  });

  help.save(function(err, help) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create Help.",
        payload: {
          help
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Help successfully created.",
      payload: {
        help
      }
    });
  });
}

//========================================
// show Help Route
//========================================
exports.fetchHelp = function(req, res, next) {
  const helpId = req.params.id;

  // Return error if no email provided
  if (!helpId) {
    return res.status(422).send({ error: 'You must enter a helpId.'});
  }

  Help.findOne({_id: helpId}, function(err, help) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Help found with the given Id.",
        payload: {}
      });
    }

    if (helpId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No Help found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Help successfully fetched.",
        payload: help
      });
    }
  });
}


//========================================
// update Help Route
//========================================
exports.updateHelp = function(req, res, next) {
  // Check for registration errors
  const title = req.body.title;
  const description = req.body.description;

  const helpId = req.params.id;

  Help.findById(helpId, function(err, help) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Help found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (title) {
      help.title = title;
    }

    // Return error if no email provided
    if (description) {
      help.description = description;
    }

    // save the Help
    help.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Help could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Help successfully updated.",
        payload: {
          help
        }
      });
    });

  });
}

//========================================
// delete Help Route
//========================================
exports.deleteHelp  = function(req, res, next) {
  const helpId = req.params.id;

  // Return error if no email provided
  if (!helpId) {
    return res.status(422).send({ error: 'You must enter a valid helpId.'});
  }

  Help.findById(helpId, function(err, help) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Help found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete Help
      Help.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete help.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Help successfully deleted.",
          payload: help
        });
      });
    } catch(e){
      //catch exception. If Help is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete Help.",
        payload: {}
      });
    }
  });
}
