"use strict";

const Payment = require('../models/payment'),
      config = require('../config/main');

//========================================
// Payment index Route
//========================================
exports.fetchAllPayments = function(req, res, next) {

  Payment.find({}, function(err, payments) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not fetch payments.",
        payload: {}
      });
    }

    res.status(200).json({
      success: true,
      message: "Payments successfully fecthed.",
      payload: payments
    });
  });
}

//========================================
// create Payment Route
//========================================
exports.createPayment = function(req, res, next) {
  // Check for schema errors
  const amount = req.body.amount;
  const _userId = req.body._userId;

  // Return error if no email provided
  if (!amount) {
    return res.status(422).send({ success: false, message: 'You must enter a valid Payment amount.', payload: {}});
  }

  // Return error if no email provided
  if (!_userId) {
    return res.status(422).send({ success: false, message: 'You must enter a valid _userId.', payload: {}});
  }

  // create record
  let Payment = new Payment({
    amount: amount,
    _userId: _userId,
  });

  Payment.save(function(err, payment) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. Could not create Payment.",
        payload: {
          payment
        }
      });
    }

    res.status(201).json({
      success: true,
      message: "Payment successfully created.",
      payload: {
        payment
      }
    });
  });
}

//========================================
// show Payment Route
//========================================
exports.fetchPayment = function(req, res, next) {
  const PaymentId = req.params.id;

  // Return error if no email provided
  if (!paymentId) {
    return res.status(422).send({ error: 'You must enter a paymentId.'});
  }

  Payment.findOne({_id: PaymentId}, function(err, payment) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Payment found with the given Id.",
        payload: {}
      });
    }

    if (paymentId == null){
      //handle empty resultset
      return res.status(422).json({
        success: false,
        message: "No Payment found with the given Id.",
        payload: {}
      });
    } else{
      // handle serach with results
      return res.status(200).json({
        success: true,
        message: "Payment successfully fetched.",
        payload: payment
      });
    }
  });
}


//========================================
// update Payment Route
//========================================
exports.updatePayment = function(req, res, next) {
  // Check for registration errors
  const amount = req.body.amount;
  const _userId = req.body._userId;

  const paymentId = req.params.id;

  Payment.findById(PaymentId, function(err, payment) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Payment found with the given Id.",
        payload: {}
      });
    }

    // Return error if no email provided
    if (amount) {
      payment.amount = amount;
    }

    // Return error if no email provided
    if (_userId) {
      payment._userId = _userId;
    }

    // save the Payment
    payment.save(function(err) {
      if (err) {
        //mongo error
        return res.status(422).json({
          success: false,
          message: "Error. Payment could not be updated.",
          payload: {}
        });
      }

      res.status(200).json({
        success: true,
        message: "Payment successfully updated.",
        payload: {
          payment
        }
      });
    });

  });
}

//========================================
// delete Payment Route
//========================================
exports.deletePayment  = function(req, res, next) {
  const paymentId = req.params.id;

  // Return error if no email provided
  if (!paymentId) {
    return res.status(422).send({ error: 'You must enter a valid PaymentId.'});
  }

  Payment.findById(PaymentId, function(err, payment) {
    if (err) {
      //mongo error
      return res.status(422).json({
        success: false,
        message: "Error. No Payment found with the given Id.",
        payload: {}
      });
    }

    try {
      // delete Payment
      payment.remove(function(err) {
        if (err) {
          //mongo error
          return res.status(422).json({
            success: false,
            message: "Error. Could not delete Payment.",
            payload: {}
          });
        }

        res.status(200).json({
          success: true,
          message: "Payment successfully deleted.",
          payload: payment
        });
      });
    } catch(e){
      //catch exception. If Payment is null
      return res.status(422).json({
        success: false,
        message: "Error. Could not delete Payment.",
        payload: {}
      });
    }
  });
}
