//=========================
// Message Routes
//=========================

/**
 * @api {post} /api/v1/messages Create new Message
 * @apiVersion 1.0.0
 * @apiName CreateMessage
 * @apiGroup Message
 *
 * @apiDescription Creates a new Message using RESTful api.
 *
 * @apiParam {String} MessageBody The body of the Message being created.
 * @apiParam {ObjectId} _senderId of the owner of the Message
 * @apiParam {ObjectId} _recipientId of the recipient of the Message
 * @apiParam {ObjectId} _threadId of the thread the Message belongs to
 * @apiParam {String} messagetatus The status of the Message. Enum with two values[READ, NOT_READ]
 * @apiParam {String} url The url of the Message
 *
 * @apiSuccess {Boolean} success  true Message creation was successful.
 * @apiSuccess {String} message  Message indicating status of Message creation.
 * @apiSuccess {String} payload  JSON object with details of created Message.
 *
 * @apiError {Boolean} success  false - the operation was not successful.
 * @apiError {String} message  Message describing the error in detail.
 * @apiError {String} payload  an empty JSON object.
 */
apiRoutes.post('/messages', requireAuth, MessagesController.createMessage);

/**
 * @api {get} /api/v1/messages Fetch all Message
 * @apiVersion 1.0.0
 * @apiName FetchAllMessages
 * @apiGroup Message
 *
 * @apiDescription Fetches all Message using RESTful api.
 *
 * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
 *                                    returned when user logged in.
 *
 * @apiSuccess {Boolean} success  true if Message fetching was successful, otherwise false.
 * @apiSuccess {String} message  Message indicating status of Message fteching.
 * @apiSuccess {String} payload  JSON object with details of all Message.
 *
 * @apiError {Boolean} success  false - the operation was not successful.
 * @apiError {String} message  Message describing the error in detail.
 * @apiError {String} payload  an empty JSON object.
 */
apiRoutes.get('/messages', requireAuth, MessagesController.fetchAllMessages);

/**
 * @api {get} /api/v1/messages:id Fetch one Message
 * @apiVersion 1.0.0
 * @apiName FetchMessage
 * @apiGroup Message
 *
 * @apiDescription Fetches a Message using RESTful api.
 *
 * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
 *                                    returned when user logged in.
 * @apiParam {String} id Message unique id.
 *
 * @apiSuccess {Boolean} success  true if Message fetching was successful, otherwise false.
 * @apiSuccess {String} message  Message indicating status of Message fteching.
 * @apiSuccess {String} payload  JSON object with details of the Message.
 *
 * @apiError {Boolean} success  false - the operation was not successful.
 * @apiError {String} message  Message describing the error in detail.
 * @apiError {String} payload  an empty JSON object.
 */
apiRoutes.get('/messages/:id', requireAuth, MessagesController.fetchMessage);

/**
 * @api {put} /api/v1/messages:id Update one Message
 * @apiVersion 1.0.0
 * @apiName UpdateMessage
 * @apiGroup Message
 *
 * @apiDescription Updates a Message using RESTful api with the provided parameters.Checks will be added later to ensure only
 * authorized users can update a record.Currently all authenticated users can.
 *
 * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
 *                                    returned when user logged in.
 *
 * @apiParam {String} id Message unique id.
 * @apiParam {String} [messageBody] The body of the Message being updated.
 * @apiParam {ObjectId} _senderId of the owner of the Message
 * @apiParam {ObjectId} _recipientId of the recipient of the Message
 * @apiParam {ObjectId} _threadId of the thread the Message belongs to
 * @apiParam {String} [messagetatus] The status of the Message. Enum with two values[READ, NOT_READ]
 *
 * @apiSuccess {Boolean} success  true if Message updating was successful, otherwise false.
 * @apiSuccess {String} message  Message indicating status of Message updating.
 * @apiSuccess {String} payload  JSON object with details of the updated Message.
 *
 * @apiError {Boolean} success  false - the operation was not successful.
 * @apiError {String} message  Message describing the error in detail.
 * @apiError {String} payload  an empty JSON object.
 */
apiRoutes.put('/messages/:id', requireAuth, MessagesController.updateMessage);

/**
 * @api {delete} /api/v1/messages:id Delete Message
 * @apiVersion 1.0.0
 * @apiName DeleteMessage
 * @apiGroup Message
 *
 * @apiDescription Deletes a Message using RESTful api.Checks will be added later to ensure only
 * authorized users can update a record.Currently all authenticated users can
 *
 * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
 *                                    returned when user logged in.
 * @apiParam {String} id Message unique id.
 *
 * @apiSuccess {Boolean} success  true if Message deleting was successful, otherwise false.
 * @apiSuccess {String} message  Message indicating status of Message deletion.
 * @apiSuccess {String} payload  JSON object with details of the deleted Message.
 *
 * @apiError {Boolean} success  false - the operation was not successful.
 * @apiError {String} message  Message describing the error in detail.
 * @apiError {String} payload  an empty JSON object.
 */
apiRoutes.delete('/messages/:id', requireAuth, MessagesController.deleteMessage);
