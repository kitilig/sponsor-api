"use strict";

const AuthenticationController = require('../controllers/authentication'),
      UsersController = require('../controllers/users'),
      PhotosController = require('../controllers/photos'),
      EventsController = require('../controllers/events'),
      HelpController = require('../controllers/help'),
      PaymentsController = require('../controllers/payments'),
      NotificationsController = require('../controllers/notifications'),
      MessagesController = require('../controllers/messages'),
      express = require('express'),
      passportService = require('../config/passport'),
      passport = require('passport');

// Middleware to require login/auth
const requireAuth = passport.authenticate('jwt', { session: false });
const requireLogin = passport.authenticate('local', { session: false });

// Constants for role types
const REQUIRE_ADMIN = "Admin",
      REQUIRE_MEMBER = "Member";


module.exports = function(app) {
  /** Initializing route groups**/
  const apiRoutes = express.Router();

  //=========================
  // User Routes
  //=========================

  /**
   * @api {post} /api/v1/users Create new user
   * @apiVersion 1.0.0
   * @apiName CreateUser
   * @apiGroup User
   * @apiDescription Creates a new user record using RESTful api.
   *
   * @apiParam {String} email Users unique email.
   * @apiParam {String} password Users password.
   * @apiParam {String} username Users username.
   * @apiParam {String} dateOfBirth Users dateOfBirth.
   *
   * @apiSuccess {Boolean} success  true User creation was successful.
   * @apiSuccess {String} message  Message indicating status of user creation.
   * @apiSuccess {String} payload  JSON object with details of created user.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/users', AuthenticationController.register);

  /**
   * @api {post} /api/v1/users/login User log in
   * @apiVersion 1.0.0
   * @apiName LoginUser
   * @apiGroup User
   * @apiDescription With correct credentials provided, it logs in a user and issues a json web token(jwt) to be used to authenticate
   * later requests. The token has a set expiry date and is to be stored by the client. To log out a user remove the jwt from the client.
   *
   * @apiParam {String} email Users unique email.
   * @apiParam {String} password Users password.
   *
   * @apiSuccess {Boolean} success  true if login was successful and false if login failed.
   * @apiSuccess {String} message  Message indicating status of user login.
   * @apiSuccess {String} payload  JSON object with details of logged in user and jwt token.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/users/login', function(req, res, next) {
    passport.authenticate('local',{ session: false }, function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { return res.status(422).send({ success: false, message: 'Invalid username or password..', payload: {}}); }
       //optimal code?
       req.user = user;
       AuthenticationController.login(req, res, next);
    })(req, res, next);
  });

   /**
    * @api {get} /api/v1/users Fetch all users
    * @apiVersion 1.0.0
    * @apiName FetchAllUsers
    * @apiGroup User
    *
    * @apiDescription Fteches all user records using RESTful api.
    *
    * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
    *                                    returned when user logged in.
    *
    * @apiSuccess {Boolean} success  true if User fetching was successful, otherwise false.
    * @apiSuccess {String} message  Message indicating status of user fteching.
    * @apiSuccess {String} payload  JSON object with details of all users.
    *
    * @apiError {Boolean} success  false - the operation was not successful.
    * @apiError {String} message  Message describing the error in detail.
    * @apiError {String} payload  an empty JSON object.
    */
  apiRoutes.get('/users', requireAuth, UsersController.fetchAllUsers);

  /**
   * @api {get} /api/v1/users:id Fetch one user
   * @apiVersion 1.0.0
   * @apiName FetchUser
   * @apiGroup User
   *
   * @apiDescription Fetches a single user record using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Users unique id.
   *
   * @apiSuccess {Boolean} success  true if User fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of user fteching.
   * @apiSuccess {String} payload  JSON object with details of the user.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/users/:id', requireAuth, UsersController.fetchUser);

  /**
   * @api {put} /api/v1/users/:id Update user
   * @apiVersion 1.0.0
   * @apiName UpdateUser
   * @apiGroup User
   *
   * @apiDescription Updates a user record using RESTful api. Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Users unique id.
   * @apiParam {String} [email] Users unique email.
   * @apiParam {String} [password] Users password.
   * @apiParam {String} [username] Users username.
   * @apiParam {String} [dateOfBirth] Users dateOfBirth.
   * @apiParam {String} [username] Users username.
   * @apiParam {String} [profile] Users profile. An arrays with the following keys {profilePicture,bodyType,ethnicity,relationshipStatus,hasChildren,smoking,drinking,age,netWorth,salaryRange,location,description,amLookingFor,exerciseFrequency,occupation}. Used by the APP
   * @apiParam {String} [likes] Users likes Array. Used by the APP
   * @apiParam {String} [views] Users views Array. Used by the APP
   * @apiParam {String} [favorites] Users favorites Array. Used by the APP
   * @apiParam {String} [winks] Users winks Array. Used by the APP
   * @apiParam {String} [matches] Users matches Array. Used by the APP
   * @apiParam {String} [dislikes] Users dislikes Array. Used by the APP
   * @apiParam {String} [settings] Users settings Array. Used by the APP
   * 
   *
   * @apiSuccess {Boolean} success  true if User updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of user fteching.
   * @apiSuccess {String} payload  JSON object with details of the updated user.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/users/:id', requireAuth, UsersController.updateUser);

  /**
   * @api {delete} /api/v1/users/:id Delete user
   * @apiVersion 1.0.0
   * @apiName DeleteUser
   * @apiGroup User
   *
   * @apiDescription Deletes a user record using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Users unique id.
   *
   * @apiSuccess {Boolean} success  true if User deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of user fteching.
   * @apiSuccess {String} payload  JSON object with details of the deleted user.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/:id', requireAuth, UsersController.deleteUser);

  //======================================================================================================
  // Sponsor app core business logic                                                                         //
  //======================================================================================================

  /**
   * @api {get} /api/v1/users/filter_users filterUsers
   * @apiVersion 1.0.0
   * @apiName filterUsers
   * @apiGroup User
   *
   * @apiDescription Filters user records per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/filter_users', requireAuth, UsersController.filterUsers);

  /**
   * @api {get} /api/v1/users/sort_users sortUsers
   * @apiVersion 1.0.0
   * @apiName sortUsers
   * @apiGroup User
   *
   * @apiDescription Sorts user records per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/sort_users', requireAuth, UsersController.sortUsers);

  /**
   * @api {get} /api/v1/users/favorite_user favoriteUser
   * @apiVersion 1.0.0
   * @apiName favoriteUser
   * @apiGroup User
   *
   * @apiDescription Allows one to favorite another user record per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/favorite_user', requireAuth, UsersController.favoriteUser);

  /**
   * @api {get} /api/v1/users/like_user likeUser
   * @apiVersion 1.0.0
   * @apiName likeUser
   * @apiGroup User
   *
   * @apiDescription Allows one to like another user per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/like_user', requireAuth, UsersController.likeUser);

  /**
   * @api {get} /api/v1/users/wink winkAtUser
   * @apiVersion 1.0.0
   * @apiName  winkAtUser
   * @apiGroup User
   *
   * @apiDescription winkAtUser per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/wink_at_user', requireAuth, UsersController.winkAtUser);

  /**
   * @api {get} /api/v1/users/user_matches userMatches
   * @apiVersion 1.0.0
   * @apiName userMatches
   * @apiGroup User
   *
   * @apiDescription userMatches records per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/user_matches', requireAuth, UsersController.userMatches);

  /**
   * @api {get} /api/v1/users/match_user matchUser
   * @apiVersion 1.0.0
   * @apiName matchUser
   * @apiGroup User
   *
   * @apiDescription  matchUser records per given criteria using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} [params] Parameters will be added later,
   *
   * @apiSuccess {Boolean} success  true if operation was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of the operation.
   * @apiSuccess {String} payload  JSON object with details of the  operation.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/users/match_user', requireAuth, UsersController.matchUser);


  //=========================
  // Photo Routes
  //=========================

  /**
   * @api {post} /api/v1/photos Create new Photo
   * @apiVersion 1.0.0
   * @apiName CreatePhoto
   * @apiGroup Photo
   *
   * @apiDescription Creates a new Photo using RESTful api.
   *
   * @apiParam {String} photoURL The url of the Photo being created.
   * @apiParam {ObjectId} _userId of the owner of the photo
   * @apiParam {String} photoType could be PRIVATE or PUBLIC
   * @apiParam {String} approvalStatus The status of the Photo. Enum with two values[APPROVED, NOT_APPROVED]
   * @apiParam {Array} likes An array with usersIds of users who have liked the photo
   *
   * @apiSuccess {Boolean} success  true Photo creation was successful.
   * @apiSuccess {String} message  Message indicating status of Photo creation.
   * @apiSuccess {String} payload  JSON object with details of created Photo.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/photos', requireAuth, PhotosController.createPhoto);

  /**
   * @api {get} /api/v1/photos Fetch all photos
   * @apiVersion 1.0.0
   * @apiName FetchAllphotos
   * @apiGroup Photo
   *
   * @apiDescription Fetches all photos using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Photo fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Photo fteching.
   * @apiSuccess {String} payload  JSON object with details of all photos.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/photos', requireAuth, PhotosController.fetchAllPhotos);

  /**
   * @api {get} /api/v1/photos:id Fetch one Photo
   * @apiVersion 1.0.0
   * @apiName FetchPhoto
   * @apiGroup Photo
   *
   * @apiDescription Fetches a Photo using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Photo unique id.
   *
   * @apiSuccess {Boolean} success  true if Photo fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Photo fteching.
   * @apiSuccess {String} payload  JSON object with details of the Photo.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/photos/:id', requireAuth, PhotosController.fetchPhoto);

  /**
   * @api {put} /api/v1/photos:id Update one Photo
   * @apiVersion 1.0.0
   * @apiName UpdatePhoto
   * @apiGroup Photo
   *
   * @apiDescription Updates a Photo using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Photo unique id.
   * @apiParam {String} [photoURL] The url of the Photo being updated.
   * @apiParam {ObjectId} [_userId] _userId of the owner of the Photo.
   * @apiParam {String} [photoType] photoType could be PUBLIC or PRIVATE.
   * @apiParam {String} approvalStatus The status of the Photo. Enum with two values[APPROVED, NOT_APPROVED]
   * @apiParam {Array} likes An array with usersIds of users who have liked the photo
   *
   * @apiSuccess {Boolean} success  true if Photo updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Photo updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Photo.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/photos/:id', requireAuth, PhotosController.updatePhoto);

  /**
   * @api {delete} /api/v1/photos:id Delete Photo
   * @apiVersion 1.0.0
   * @apiName DeletePhoto
   * @apiGroup Photo
   *
   * @apiDescription Deletes a Photo using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Photo unique id.
   *
   * @apiSuccess {Boolean} success  true if Photo deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Photo deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Photo.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/photos/:id', requireAuth, PhotosController.deletePhoto);


  //=========================
  // Event Routes
  //=========================

  /**
   * @api {post} /api/v1/events Create new Event
   * @apiVersion 1.0.0
   * @apiName CreateEvent
   * @apiGroup Event
   *
   * @apiDescription Creates a new Event using RESTful api.
   *
   * @apiParam {String} eventTitle The title of the Event being created.
   * @apiParam {String} eventVenue Venue of  of the Event
   * @apiParam {String} eventType Event type could be FREE or INVITE_ONLY or OTHER
   * @apiParam {Date} eventDate The date of the Event.
   * @apiParam {String} eventBannerURL The url of the event banner.
   *
   * @apiSuccess {Boolean} success  true Event creation was successful.
   * @apiSuccess {String} message  Message indicating status of Event creation.
   * @apiSuccess {String} payload  JSON object with details of created Event.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/events', requireAuth, EventsController.createEvent);

  /**
   * @api {get} /api/v1/events Fetch all Events
   * @apiVersion 1.0.0
   * @apiName FetchAllEvents
   * @apiGroup Event
   *
   * @apiDescription Fetches all Events using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Event fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Event fteching.
   * @apiSuccess {String} payload  JSON object with details of all Events.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/events', requireAuth, EventsController.fetchAllEvents);

  /**
   * @api {get} /api/v1/events:id Fetch one Event
   * @apiVersion 1.0.0
   * @apiName FetchEvent
   * @apiGroup Event
   *
   * @apiDescription Fetches a Event using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Event unique id.
   *
   * @apiSuccess {Boolean} success  true if Event fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Event fteching.
   * @apiSuccess {String} payload  JSON object with details of the Event.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/events/:id', requireAuth, EventsController.fetchEvent);

  /**
   * @api {put} /api/v1/events:id Update one Event
   * @apiVersion 1.0.0
   * @apiName UpdateEvent
   * @apiGroup Event
   *
   * @apiDescription Updates a Event using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Event unique id.
   * @apiParam {String} [eventTitle] The title of the Event being updated.
   * @apiParam {String} [eventVenue] Venue of the Event.
   * @apiParam {String} [EventType] EventType could be FREE or INVITE_ONLY or OTHER
   * @apiParam {Date} [eventDate] The date of the Event.
   * @apiParam {String} [eventBannerURL] The url of the event banner.
   *
   * @apiSuccess {Boolean} success  true if Event updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Event updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Event.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/events/:id', requireAuth, EventsController.updateEvent);

  /**
   * @api {delete} /api/v1/events:id Delete Event
   * @apiVersion 1.0.0
   * @apiName DeleteEvent
   * @apiGroup Event
   *
   * @apiDescription Deletes a Event using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Event unique id.
   *
   * @apiSuccess {Boolean} success  true if Event deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Event deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Event.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/events/:id', requireAuth, EventsController.deleteEvent);

  //=========================
  // Help Routes
  //=========================

  /**
   * @api {post} /api/v1/help Create new Help
   * @apiVersion 1.0.0
   * @apiName CreateHelp
   * @apiGroup Help
   *
   * @apiDescription Creates a new Help using RESTful api.
   *
   * @apiParam {String} title The title of the Help being created.
   * @apiParam {String} description Description of  of the Help item
   * 
   * @apiSuccess {Boolean} success  true Help creation was successful.
   * @apiSuccess {String} message  Message indicating status of Help creation.
   * @apiSuccess {String} payload  JSON object with details of created Help.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/help', requireAuth, HelpController.createHelp);

  /**
   * @api {get} /api/v1/help Fetch all Helps
   * @apiVersion 1.0.0
   * @apiName FetchAllHelps
   * @apiGroup Help
   *
   * @apiDescription Fetches all Helps using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Help fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Help fteching.
   * @apiSuccess {String} payload  JSON object with details of all Helps.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/help', requireAuth, HelpController.fetchAllHelp);

  /**
   * @api {get} /api/v1/help:id Fetch one Help
   * @apiVersion 1.0.0
   * @apiName FetchHelp
   * @apiGroup Help
   *
   * @apiDescription Fetches a Help using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Help unique id.
   *
   * @apiSuccess {Boolean} success  true if Help fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Help fteching.
   * @apiSuccess {String} payload  JSON object with details of the Help.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/help/:id', requireAuth, HelpController.fetchHelp);

  /**
   * @api {put} /api/v1/help:id Update one Help
   * @apiVersion 1.0.0
   * @apiName UpdateHelp
   * @apiGroup Help
   *
   * @apiDescription Updates a Help using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Help unique id.
   * @apiParam {String} [title] The title of the Help being updated.
   * @apiParam {String} [description] Description of the Help item
   *
   * @apiSuccess {Boolean} success  true if Help updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Help updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Help.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/help/:id', requireAuth, HelpController.updateHelp);

  /**
   * @api {delete} /api/v1/help:id Delete Help
   * @apiVersion 1.0.0
   * @apiName DeleteHelp
   * @apiGroup Help
   *
   * @apiDescription Deletes a Help using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Help unique id.
   *
   * @apiSuccess {Boolean} success  true if Help deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Help deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Help.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/help/:id', requireAuth, HelpController.deleteHelp);

  //=========================
  // Payment Routes
  //=========================

  /**
   * @api {post} /api/v1/payments Create new Payment
   * @apiVersion 1.0.0
   * @apiName CreatePayment
   * @apiGroup Payment
   *
   * @apiDescription Creates a new Payment using RESTful api.
   *
   * @apiParam {Number} amount The amount of the Payment being created.
   * @apiParam {ObjectId} _userId _userId of  of the Payment item
   * 
   * @apiSuccess {Boolean} success  true Payment creation was successful.
   * @apiSuccess {String} message  Message indicating status of Payment creation.
   * @apiSuccess {String} payload  JSON object with details of created Payment.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/payments', requireAuth, PaymentsController.createPayment);

  /**
   * @api {get} /api/v1/payments Fetch all Payments
   * @apiVersion 1.0.0
   * @apiName FetchAllPayments
   * @apiGroup Payment
   *
   * @apiDescription Fetches all Payments using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Payment fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Payment fteching.
   * @apiSuccess {String} payload  JSON object with details of all Payments.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/payments', requireAuth, PaymentsController.fetchAllPayments);

  /**
   * @api {get} /api/v1/payment:id Fetch one Payment
   * @apiVersion 1.0.0
   * @apiName FetchPayment
   * @apiGroup Payment
   *
   * @apiDescription Fetches a Payment using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Payment unique id.
   *
   * @apiSuccess {Boolean} success  true if Payment fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Payment fteching.
   * @apiSuccess {String} payload  JSON object with details of the Payment.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/payments/:id', requireAuth, PaymentsController.fetchPayment);

  /**
   * @api {put} /api/v1/payments:id Update one Payment
   * @apiVersion 1.0.0
   * @apiName UpdatePayment
   * @apiGroup Payment
   *
   * @apiDescription Updates a Payment using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Payment unique id.
   * @apiParam {Number} [amount] The amount of the Payment being updated.
   * @apiParam {ObjectId} [_userId] _userId of the Payment item
   *
   * @apiSuccess {Boolean} success  true if Payment updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Payment updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Payment.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/payments/:id', requireAuth, PaymentsController.updatePayment);

  /**
   * @api {delete} /api/v1/payments:id Delete Payment
   * @apiVersion 1.0.0
   * @apiName DeletePayment
   * @apiGroup Payment
   *
   * @apiDescription Deletes a Payment using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Payment unique id.
   *
   * @apiSuccess {Boolean} success  true if Payment deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Payment deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Payment.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/payments/:id', requireAuth, PaymentsController.deletePayment);

  //=========================
  // Notification Routes
  //=========================

  /**
   * @api {post} /api/v1/notifications Create new Notification
   * @apiVersion 1.0.0
   * @apiName CreateNotification
   * @apiGroup Notification
   *
   * @apiDescription Creates a new Notification using RESTful api.
   *
   * @apiParam {String} notificationBody The body of the Notification being created.
   * @apiParam {ObjectId} _userId of the owner of the Notification
   * @apiParam {String} notificationType could be PRIVATE or PUBLIC
   * @apiParam {String} notificationStatus The status of the Notification. Enum with two values[READ, NOT_READ]
   * @apiParam {String} url The url of the notification
   *
   * @apiSuccess {Boolean} success  true Notification creation was successful.
   * @apiSuccess {String} message  Message indicating status of Notification creation.
   * @apiSuccess {String} payload  JSON object with details of created Notification.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/notifications', requireAuth, NotificationsController.createNotification);

  /**
   * @api {get} /api/v1/notifications Fetch all Notifications
   * @apiVersion 1.0.0
   * @apiName FetchAllNotifications
   * @apiGroup Notification
   *
   * @apiDescription Fetches all Notifications using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Notification fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Notification fteching.
   * @apiSuccess {String} payload  JSON object with details of all Notifications.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/notifications', requireAuth, NotificationsController.fetchAllNotifications);

  /**
   * @api {get} /api/v1/notifications:id Fetch one Notification
   * @apiVersion 1.0.0
   * @apiName FetchNotification
   * @apiGroup Notification
   *
   * @apiDescription Fetches a Notification using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Notification unique id.
   *
   * @apiSuccess {Boolean} success  true if Notification fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Notification fteching.
   * @apiSuccess {String} payload  JSON object with details of the Notification.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/notifications/:id', requireAuth, NotificationsController.fetchNotification);

  /**
   * @api {put} /api/v1/notifications:id Update one Notification
   * @apiVersion 1.0.0
   * @apiName UpdateNotification
   * @apiGroup Notification
   *
   * @apiDescription Updates a Notification using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Notification unique id.
   * @apiParam {String} [notificationBody] The body of the Notification being updated.
   * @apiParam {ObjectId} [_userId] _userId of the owner of the Notification.
   * @apiParam {String} [notificationType] NotificationType could be PUBLIC or PRIVATE.
   * @apiParam {String} [notificationStatus] The status of the Notification. Enum with two values[READ, NOT_READ]
   * @apiParam {String} url The url of the notification.
   *
   * @apiSuccess {Boolean} success  true if Notification updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Notification updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Notification.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/notifications/:id', requireAuth, NotificationsController.updateNotification);

  /**
   * @api {delete} /api/v1/notifications:id Delete Notification
   * @apiVersion 1.0.0
   * @apiName DeleteNotification
   * @apiGroup Notification
   *
   * @apiDescription Deletes a Notification using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Notification unique id.
   *
   * @apiSuccess {Boolean} success  true if Notification deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Notification deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Notification.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/notifications/:id', requireAuth, NotificationsController.deleteNotification);

  //=========================
  // Message Routes
  //=========================

  /**
   * @api {post} /api/v1/messages Create new Message
   * @apiVersion 1.0.0
   * @apiName CreateMessage
   * @apiGroup Message
   *
   * @apiDescription Creates a new Message using RESTful api.
   *
   * @apiParam {String} MessageBody The body of the Message being created.
   * @apiParam {ObjectId} _senderId of the owner of the Message
   * @apiParam {ObjectId} _recipientId of the recipient of the Message
   * @apiParam {ObjectId} _threadId of the thread the Message belongs to
   * @apiParam {String} messagetatus The status of the Message. Enum with two values[READ, NOT_READ]
   * @apiParam {String} url The url of the Message
   *
   * @apiSuccess {Boolean} success  true Message creation was successful.
   * @apiSuccess {String} message  Message indicating status of Message creation.
   * @apiSuccess {String} payload  JSON object with details of created Message.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.post('/messages', requireAuth, MessagesController.createMessage);

  /**
   * @api {get} /api/v1/messages Fetch all Message
   * @apiVersion 1.0.0
   * @apiName FetchAllMessages
   * @apiGroup Message
   *
   * @apiDescription Fetches all Message using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiSuccess {Boolean} success  true if Message fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Message fteching.
   * @apiSuccess {String} payload  JSON object with details of all Message.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/messages', requireAuth, MessagesController.fetchAllMessages);

  /**
   * @api {get} /api/v1/messages:id Fetch one Message
   * @apiVersion 1.0.0
   * @apiName FetchMessage
   * @apiGroup Message
   *
   * @apiDescription Fetches a Message using RESTful api.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Message unique id.
   *
   * @apiSuccess {Boolean} success  true if Message fetching was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Message fteching.
   * @apiSuccess {String} payload  JSON object with details of the Message.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.get('/messages/:id', requireAuth, MessagesController.fetchMessage);

  /**
   * @api {put} /api/v1/messages:id Update one Message
   * @apiVersion 1.0.0
   * @apiName UpdateMessage
   * @apiGroup Message
   *
   * @apiDescription Updates a Message using RESTful api with the provided parameters.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can.
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   *
   * @apiParam {String} id Message unique id.
   * @apiParam {String} [messageBody] The body of the Message being updated.
   * @apiParam {ObjectId} _senderId of the owner of the Message
   * @apiParam {ObjectId} _recipientId of the recipient of the Message
   * @apiParam {ObjectId} _threadId of the thread the Message belongs to
   * @apiParam {String} [messagetatus] The status of the Message. Enum with two values[READ, NOT_READ]
   *
   * @apiSuccess {Boolean} success  true if Message updating was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Message updating.
   * @apiSuccess {String} payload  JSON object with details of the updated Message.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.put('/messages/:id', requireAuth, MessagesController.updateMessage);

  /**
   * @api {delete} /api/v1/messages:id Delete Message
   * @apiVersion 1.0.0
   * @apiName DeleteMessage
   * @apiGroup Message
   *
   * @apiDescription Deletes a Message using RESTful api.Checks will be added later to ensure only
   * authorized users can update a record.Currently all authenticated users can
   *
   * @apiParam {String} AuthorizationHeader The Authorization header needs to be set with the jwt token
   *                                    returned when user logged in.
   * @apiParam {String} id Message unique id.
   *
   * @apiSuccess {Boolean} success  true if Message deleting was successful, otherwise false.
   * @apiSuccess {String} message  Message indicating status of Message deletion.
   * @apiSuccess {String} payload  JSON object with details of the deleted Message.
   *
   * @apiError {Boolean} success  false - the operation was not successful.
   * @apiError {String} message  Message describing the error in detail.
   * @apiError {String} payload  an empty JSON object.
   */
  apiRoutes.delete('/messages/:id', requireAuth, MessagesController.deleteMessage);

  // Set url for API group routes
  app.use('/api/v1', apiRoutes);
};
