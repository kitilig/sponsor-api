Sponsor API
===========

A dating app that fits the current meaning of the word “sponsor” in a Kenyan context. 
The framework should be similar to that of other dating apps but differing in various 
approaches.

## Technology Stack
+ React.js for the web application
+ React-native  - for the mobile app
+ Node/Express/Mongo - backend/API

Combined effort project.

Powered by TrendPro Systems Limited
